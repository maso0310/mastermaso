from django.apps import AppConfig


class TreasureMapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Treasure_Map'
