from django.contrib import admin

# Register your models here.
from .models import *

#紀錄會員資料
class User_Info_Admin(admin.ModelAdmin):
    list_display = ('id','uid','name','email','pic_url','status','notify_token','mdt')
admin.site.register(User_Info,User_Info_Admin)

#紀錄座標訊息
class Location_Info_Admin(admin.ModelAdmin):
    list_display = ('id','uid','name','title','address','lat','lng','text','image_url','mdt')
admin.site.register(Location_Info,Location_Info_Admin)

#紀錄錯誤訊息
class Error_Logs_Admin(admin.ModelAdmin):
    list_display = ('id','uid','name','webhook','error','mdt')
admin.site.register(Error_Logs,Error_Logs_Admin)

