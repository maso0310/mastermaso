from django.db import models

# Create your models here.
#建立會員資料
class User_Info(models.Model):
    uid = models.CharField(max_length=50,null=False,default='')                           #user_id
    name = models.CharField(max_length=255,blank=True,null=False)                         #LINE名字
    email = models.CharField(max_length=255,blank=True,null=False,default='')             #email
    pic_url = models.CharField(max_length=255,null=False)                                 #大頭貼網址
    status = models.CharField(max_length=255,blank=True,null=False,default='0')           #使用者狀態
    notify_token = models.CharField(max_length=255,blank=True,null=False,default='')      #LINE Notify的token
    mdt = models.DateTimeField(auto_now=True)                                             #物件儲存的日期時間

    def __str__(self):
        return self.uid

class Location_Info(models.Model):
    uid = models.CharField(max_length=50,blank=True,null=True)                            #user_id
    name = models.CharField(max_length=255,blank=True,null=True)                          #LINE名字
    title = models.CharField(max_length=255,blank=True,null=True)                         #地標名稱
    address = models.CharField(max_length=255,blank=True,null=True)                       #地標地址
    lat = models.CharField(max_length=255,blank=True,null=True)                           #地標緯度
    lng = models.CharField(max_length=255,blank=True,null=True)                           #地標經度
    text = models.CharField(max_length=255,blank=True,null=True,default='')               #地標文字說明
    image_url = models.CharField(max_length=255,blank=True,null=True,default='')          #地標圖片資訊
    mdt = models.DateTimeField(auto_now=True)                                             #物件儲存的日期時間

    def __str__(self):
        return self.uid
    
#建立錯誤資料
class Error_Logs(models.Model):
    uid = models.CharField(max_length=50,blank=True,null=True)                            #user_id
    name = models.CharField(max_length=255,blank=True,null=True)                          #LINE名字
    webhook = models.CharField(max_length=5000,blank=True,null=True,default='')           #發生錯誤的webhook event
    error = models.CharField(max_length=255,null=False,default='')                        #error內容
    mdt = models.DateTimeField(auto_now=True)                                             #物件儲存的日期時間

    def __str__(self):
        return self.error
